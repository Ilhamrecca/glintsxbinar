const { stack } = require("../Day 5/routes/TransaksiRoute");

const Calculator = function (input) {
	console.log(input);
	input = input.split(" ");
	let result = convertToPostFix(input);

	return parseInt(evaluate(result));
};

const evaluate = function (input) {
	let stack = [];
	for (let i = 0; i < input.length; i++) {
		if (!isOperand(input[i])) {
			stack.push(input[i]);
		} else {
			num1 = stack.pop();
			num2 = stack.pop();
			stack.push(calculates(num2, num1, input[i]));
		}
	}
	return stack.pop();
};

const calculates = function (num1, num2, operation) {
	num1 = parseInt(num1);
	num2 = parseInt(num2);
	switch (operation) {
		case "+":
			return num1 + num2;
		case "-":
			return num1 - num2;
		case "*":
			return num1 * num2;
		case "/":
			return num1 / num2;
		case "^":
			return num1 ** num2;
	}
};

const getPrecedence = function (input) {
	switch (input) {
		case "+":
			return 1;
		case "-":
			return 1;
		case "*":
			return 2;
		case "/":
			return 2;
		case "^":
			return 3;
		case "(":
			return 0;
		case ")":
			return 0;
	}
};

const isOperand = function (input) {
	if (
		input === "/" ||
		input === "+" ||
		input === "-" ||
		input === "*" ||
		input === "^" ||
		input === "(" ||
		input === ")"
	) {
		return true;
	}
	return false;
};

const greaterPrecedence = function (operator1, operator2) {
	return getPrecedence(operator1) - getPrecedence(operator2);
};

const convertToPostFix = function (input) {
	console.log(input);
	let stack = [];
	let result = [];
	for (let i = 0; i < input.length; i++) {
		if (!isOperand(input[i])) {
			result.push(input[i]);
		} else if (input[i] === "(") {
			stack.push(input[i]);
		} else if (input[i] === ")") {
			topStack = stack.pop();
			while (topStack !== "(") {
				result.push(topStack);
				topStack = stack.pop();
			}
			// stack.pop();
		} else if (
			greaterPrecedence(input[i], stack[stack.length - 1]) > 0 ||
			stack.length < 1
		) {
			stack.push(input[i]);
		} else if (greaterPrecedence(input[i], stack[stack.length - 1]) < 0) {
			for (let i = stack.length - 1; i >= 0; i--) {
				topStack = stack.pop();
				result.push(topStack);
				console.log(stack);
				if (greaterPrecedence(input[i], topStack) > 0) {
					stack.push(topStack);
					// stack.push(input[i]);
					result.pop();
					break;
				}
			}
			stack.push(input[i]);
		} else if (greaterPrecedence(input[i], stack[stack.length - 1]) === 0) {
			if (input[i] === "^") {
				stack.push(input[i]);
			} else {
				for (let i = stack.length - 1; i >= 0; i--) {
					topStack = stack.pop();
					result.push(topStack);
					if (greaterPrecedence(input[i], topStack) > 1) {
						stack.push(topStack);
						// stack.push(input[i]);
						result.pop();
						break;
					}
				}
				stack.push(input[i]);
			}
		}
		// console.log("stack = " + stack);
	}

	while (stack.length > 0) {
		result.push(stack.pop());
	}

	return result;
};

// // console.log("result = " + convertToPostFix("()"));

// console.log(Calculator("10 * 5 / 2"));
// console.log(Calculator("10 + 6 / 2"));

// Test Function do not edit
function Test(fun, result) {
	console.log(fun === result);
}
Test(Calculator("( 10 + 6 ) / 2"), 8);
Test(Calculator("127"), 127);
Test(Calculator("2 + 3"), 5);
Test(Calculator("2 - 3 - 4"), -5);
Test(Calculator("10 * 5 / 2"), 25);

console.log("Bonus Test");
Test(Calculator("10 + 6 / 2"), 13);

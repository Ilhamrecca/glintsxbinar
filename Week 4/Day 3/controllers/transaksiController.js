const connection = require("../models/connection.js"); // import connection
class TransaksiController {
	async getAll(req, res) {
		try {
			var sql =
				"SELECT mahasiswa.noBP, nama_mhs, nilai_uts FROM mahasiswa INNER JOIN nilai ON mahasiswa.noBP=nilai.noBP"; // make an query varible

			// Run query
			connection.query(sql, function (err, result) {
				if (err) throw err; // If error

				// If success it will return JSON of result
				res.json({
					status: "success",
					data: result,
				});
			});
		} catch (e) {
			// If error will be send Error JSON
			res.json({
				status: "Error",
			});
		}
	}

	async getOne(req, res) {
		try {
			var sql =
				"SELECT mahasiswa.noBP, nama_mhs, nilai_uts FROM mahasiswa INNER JOIN nilai ON mahasiswa.noBP=nilai.noBP WHERE mahasiswa.nama_mhs = ?;"; // make an query varible

			// Run query
			connection.query(sql, [req.params.id], function (err, result) {
				if (err) throw err; // If error

				// If success it will return JSON of result
				res.json({
					status: "success",
					data: result[0],
				});
			});
		} catch (e) {
			// If error will be send Error JSON
			res.json({
				status: "Error",
			});
		}
	}

	async create(req, res) {
		try {
			connection.query(
				"INSERT INTO fakultas (kode_f, nama_PS) VALUES (?, ?)",
				[req.body.kode_f, req.body.nama_PS],
				(error, result) => {
					if (error) throw error; // If error

					// If success it will return JSON of result
					res.json({
						status: "Success",
						data: result,
					});
				}
			);
		} catch (e) {
			// If error will be send Error JSON

			res.json({
				status: "Error",
			});
		}
	}

	async update(req, res) {
		try {
			connection.query(
				"UPDATE nilai SET nilai_pr = ?, nilai_kuis = ?, nilai_uts = ?, nilai_uas = ? WHERE noBP = ? and kd_mk = ?",
				[
					req.body.nilai_pr,
					req.body.nilai_kuis,
					req.body.nilai_uts,
					req.body.nilai_uas,
					req.params.id,
					req.params.kd_mk,
				],
				(error, result) => {
					if (error) throw error; // If error

					// If success it will return JSON of result
					res.json({
						status: "Success",
						data: result,
					});
				}
			);
		} catch (e) {
			// If error will be send Error JSON
			res.json({
				status: "Error",
			});
		}
	}
	async delete(req, res) {
		try {
			connection.query(
				"DELETE FROM fakultas WHERE kode_f = ?",
				[req.params.id],
				(error, result) => {
					if (error) throw error; // If error

					// If success it will return JSON of result
					res.json({
						status: "Success",
						data: result,
					});
				}
			);
		} catch (e) {
			// If error will be send Error JSON
			res.json({
				status: "Error",
			});
		}
	}
}

module.exports = new TransaksiController();

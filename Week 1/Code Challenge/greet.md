This Code is going to take user's Name, City, and birthday year and output their Name, City where the live and their age according to their birthday year. 


```bash
function greet(name, address, birthday) {
    const currentDate = new Date();
    const currentYear = currentDate.getFullYear();

    console.log(`Hello ${name} Look like you're ${currentYear-birthday}, and you Lived in ${address}` );
}
```
The above code is greet function, which takes 3 argument, name, address and birthday.
inside this function, we initialize the current date, and current year from the currentDate object.

and then, using literal template, we can substract the currentYear, with user's birthday to get their age then finally output it to the console.

```bash
console.log("Goverment Registry\n")
// GET User's Name
rl.question("What is your name? ",  name => { 
  // GET User's Address
  rl.question("Which city do you live? ", address => {
    // GET User's Birthday
    rl.question("When was your birthday year? ", birthday => {
      greet(name, address, birthday)

      rl.close()
    })
  })
})
```
As for these code, their function is to ask for user input and then call the greet function with user input as the parameter.
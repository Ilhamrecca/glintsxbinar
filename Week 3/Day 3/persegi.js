const bangundatar = require('./bangundatar.js')

class Persegi extends bangundatar {

  constructor(sisi) {
    super('Persegi')
    this.sisi = sisi
  }

  // Overriding
  menghitungLuas() {
    return this.sisi ** 2
  }

  menghitungKeliling() {
    return this.sisi * 4
  }
}

module.exports = Persegi

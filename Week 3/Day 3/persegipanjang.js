const bangundatar = require('./bangundatar.js')

// This class is child of BangunDatar
class PersegiPanjang extends bangundatar {

  constructor(panjang, lebar) {
    super('Persegi Panjang')
    this.panjang = panjang
    this.lebar = lebar
  }

  // Overriding of BangunDatar
  menghitungLuas() {
    return this.panjang * this.lebar
  }

  menghitungKeliling() {
    return 2 * (this.panjang + this.lebar)
  }
}

module.exports = PersegiPanjang

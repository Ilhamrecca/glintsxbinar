const { exception } = require("console");

class ilhamController {
	async hello(req, res) {
		try {
			console.log("you're accessing Ilham");
			res.render("ilham.ejs");
		} catch (error) {
			res.status(500).send(exception);
			console.log(error);
		}
	}

	async home(req, res) {
		try {
			console.log("you're accessing home");
			res.render("home.ejs");
		} catch (error) {
			res.status(500).send(exception);
			console.log(error);
		}
	}
}

module.exports = new ilhamController();

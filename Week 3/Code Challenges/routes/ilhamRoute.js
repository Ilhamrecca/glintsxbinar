const express = require("express");

const router = express.Router();
const ilhamController = require("../controllers/ilhamController.js");

router.get("/ilham", ilhamController.hello);
router.get("/", ilhamController.home);
module.exports = router;

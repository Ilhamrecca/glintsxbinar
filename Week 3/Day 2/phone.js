const phone = require(`./Mobile.js`);

phone.introduce();

// console.log(phone.screen) it will print "true"
phone.fall();

const iphone = new phone("iPhone 12 Mini", "Apple", 1000000, {
	ram: "4 GB",
	processor: "Apple A14",
});
iphone.ad();
iphone.banting(); // cause instance method
// console.log(iphone.screen) // it will be failed cause of static method

console.log(iphone instanceof phone); // true, cause of instance of phone

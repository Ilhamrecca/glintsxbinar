class MobilePhone {
	static screen = true; // Static variable

	// Constructor
	constructor(name, brand, price, specs) {
		this.name = name;
		this.brand = brand;
		this.price = price;
		this.specs = specs;
	}

	// Static Method/Function
	static introduce() {
		console.log(`This is Phone`);
	}

	/* Intance Method */
	hello() {
		console.log(`I am ${this.name} from ${this.brand}`);
	}

	myspecs() {
		console.log(
			`This is my spec, ${this.specs.ram} RAM and ${this.specs.processor} Processor`
		);
	}

	myprice() {
		console.log(`My price is IDR ${this.price} `);
	}

	ad() {
		// this.introduce() // It will be error cause of static
		this.hello();
		this.myspecs();
		this.myprice();
	}
	/* End instance method */
}

// Add Instance method
MobilePhone.prototype.banting = function () {
	console.log("Pecah dong!");
};

// Addn Static method
MobilePhone.fall = function () {
	console.log("Rusak dong!");
};

// Export Class
module.exports = MobilePhone;

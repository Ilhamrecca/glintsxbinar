const express = require("express");
const app = express();
const hello = require("./routes/helloRoute.js");
app.use(express.static("public"));

app.get("/", (req, res) => {
	res.render("top.ejs");
});

app.use("/", hello);

app.get("/index", (req, res) => {
	res.render("index.ejs");
});

app.listen(3000);

const express = require("express");

const router = express.Router();
const helloController = require("../controllers/helloController.js");

router.get("/ilham", helloController.hello);

module.exports = router;

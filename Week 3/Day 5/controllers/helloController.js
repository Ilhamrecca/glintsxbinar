const { exception } = require("console");

class HelloController {
	async hello(req, res) {
		try {
			console.log("you're accessing hellow");
			res.render("hello.ejs");
		} catch (error) {
			res.status(500).send(exception);
		}
	}
}

module.exports = new HelloController();
